"use strict";
var common_vendor = require("../../common/vendor.js");
var utils_utils = require("../../utils/utils.js");
var api_home = require("../../api/home.js");
require("../../utils/http.js");
require("../../utils/navigate.js");
const _sfc_main = {
  setup() {
    const state = common_vendor.reactive({
      prodCount: null,
      tagList: null,
      topNoticeList: [],
      indexImage: [],
      tagShopList: []
    });
    const methods = {
      hanldeImageClick() {
        common_vendor.index.bw_navgiate({
          url: "/pages/address/add"
        });
      },
      handleToDetail(item) {
        common_vendor.index.bw_navgiate({
          url: "/pages/detail/index",
          params: {
            prodId: item.prodId
          }
        });
      }
    };
    const init = async () => {
      common_vendor.index.showLoading({ mask: true, title: "\u52A0\u8F7D\u4E2D" });
      const [prodCount, tagList, topNoticeList, indexImage] = await Promise.allSettled([api_home.getProdCount(), api_home.getTagList(), api_home.getTopNoticeList(), api_home.getIndexImage()]);
      state.prodCount = prodCount.value;
      state.tagList = tagList.value;
      state.topNoticeList = topNoticeList.value;
      state.indexImage = indexImage.value;
      prodCount.value && utils_utils.setShopCarIconText(prodCount.value);
      const res = await Promise.allSettled(tagList.value.map(({ id }) => api_home.getProdListByTagId({
        tagId: id,
        size: 6
      })));
      state.tagShopList = res;
      console.log(res);
      common_vendor.index.hideLoading();
    };
    common_vendor.onMounted(() => {
      console.log("onMounted---");
      init();
    });
    return {
      ...methods,
      ...common_vendor.toRefs(state)
    };
  }
};
if (!Array) {
  const _component_base_shop_Item = common_vendor.resolveComponent("base-shop-Item");
  _component_base_shop_Item();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.f(_ctx.indexImage, (val, k0, i0) => {
      return {
        a: val.imgUrl,
        b: val.seq,
        c: common_vendor.o((...args) => _ctx.hanldeImageClick && _ctx.hanldeImageClick(...args), val.seq)
      };
    }),
    b: common_vendor.f(_ctx.tagList, (val, index, i0) => {
      var _a, _b;
      return common_vendor.e({
        a: common_vendor.t(val.title),
        b: ((_a = _ctx.tagShopList[index]) == null ? void 0 : _a.status) === "fulfilled"
      }, ((_b = _ctx.tagShopList[index]) == null ? void 0 : _b.status) === "fulfilled" ? {
        c: common_vendor.f(_ctx.tagShopList[index].value.records, (item, k1, i1) => {
          return {
            a: item.prodId,
            b: common_vendor.o(($event) => _ctx.handleToDetail(item), item.prodId),
            c: "3c5f9190-0-" + i0 + "-" + i1,
            d: common_vendor.p({
              ["shop-item-data"]: item
            })
          };
        })
      } : {}, {
        d: val.id
      });
    })
  };
}
var MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/yihang/Desktop/1909A/bw-wxshop-client/src/pages/index/index.vue"]]);
wx.createPage(MiniProgramPage);
