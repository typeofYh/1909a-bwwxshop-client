"use strict";
var common_vendor = require("../../common/vendor.js");
var api_live = require("../../api/live.js");
require("../../utils/http.js");
require("../../utils/navigate.js");
const _sfc_main = {
  setup() {
    const state = common_vendor.reactive({
      liveList: []
    });
    const methods = {
      hanldOpenLive(roomId) {
        let customParams = encodeURIComponent(JSON.stringify({ path: "pages/liveShop/index", pid: roomId }));
        wx.navigateTo({
          url: `plugin-private://wx2b03c6e691cd7370/pages/live-player-plugin?room_id=${roomId}&custom_params=${customParams}`
        });
      }
    };
    const init = async () => {
      const { data } = await api_live.getLists();
      state.liveList = data.room_info;
    };
    common_vendor.onMounted(() => {
      init();
    });
    return {
      ...common_vendor.toRefs(state),
      ...methods
    };
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.f(_ctx.liveList, (val, k0, i0) => {
      return {
        a: common_vendor.t(val.name),
        b: val.roomid,
        c: common_vendor.o(($event) => _ctx.hanldOpenLive(val.roomid), val.roomid)
      };
    })
  };
}
var MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/yihang/Desktop/1909A/bw-wxshop-client/src/pages/liveShop/index.vue"]]);
wx.createPage(MiniProgramPage);
