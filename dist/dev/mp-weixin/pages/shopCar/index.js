"use strict";
var common_vendor = require("../../common/vendor.js");
var api_shopcar = require("../../api/shopcar.js");
require("../../utils/http.js");
require("../../utils/navigate.js");
const ShopCarList = () => "./components/shopCarList.js";
const _sfc_main = {
  components: {
    ShopCarList
  },
  setup() {
    const state = common_vendor.reactive({
      shopCarInfo: [],
      totalMoney: 0,
      allChecked: false,
      status: 0
    });
    const methods = {
      goHome() {
        common_vendor.index.bw_navgiate({
          url: "/pages/index/index",
          replace: true
        });
      },
      async getTotalPay(checkeds) {
        const { totalMoney } = await api_shopcar.shopCarTotalPay(checkeds);
        state.totalMoney = totalMoney;
      },
      handleChange(e) {
        const curItem = state.shopCarInfo.find((item) => item.basketId === e);
        curItem.checked = !curItem.checked;
        state.allChecked = state.shopCarInfo.every((item) => item.checked);
      },
      handleAllCheckedChange() {
        state.allChecked = !state.allChecked;
        state.shopCarInfo = state.shopCarInfo.map((item) => ({ ...item, checked: state.allChecked }));
      },
      async handleChangeCount({ count, itemData: { prodId, shopId, skuId } }) {
        common_vendor.index.showLoading();
        try {
          await api_shopcar.shopCarChangeItem({
            count,
            prodId,
            shopId,
            skuId
          });
          const items = state.shopCarInfo.find((item) => item.prodId === prodId);
          items.prodCount = items.prodCount + count;
        } catch (error) {
          common_vendor.index.showToast({
            icon: "none",
            title: error
          });
        } finally {
          common_vendor.index.hideLoading();
        }
      },
      goOrderPage() {
        const isChecked = state.shopCarInfo.some((item) => item.checked);
        if (!isChecked) {
          common_vendor.index.showToast({
            title: "\u8BF7\u9009\u62E9\u5546\u54C1",
            icon: "none"
          });
          return;
        }
        common_vendor.index.bw_navgiate({
          url: "/pages/order/index",
          params: {
            basketIds: state.shopCarInfo.filter((item) => item.checked).map((item) => item.basketId)
          }
        });
      }
    };
    common_vendor.watch(() => state.shopCarInfo, (val) => {
      const checkeds = val.filter((item) => item.checked).map((item) => item.basketId);
      methods.getTotalPay(checkeds);
    }, {
      immediate: true,
      deep: true
    });
    const init = async () => {
      try {
        const data = await api_shopcar.shopCarInfo();
        if (Array.isArray(data) && data.length === 0) {
          state.status = 1;
          return;
        }
        const [{ shopCartItemDiscounts: [{ shopCartItems }] }] = data;
        state.status = 2;
        state.shopCarInfo = shopCartItems.map((item) => ({ ...item, checked: false }));
      } catch (error) {
        console.error(error);
      }
    };
    common_vendor.onMounted(() => {
      init();
    });
    return {
      ...common_vendor.toRefs(state),
      ...methods
    };
  }
};
if (!Array) {
  const _component_base_empty = common_vendor.resolveComponent("base-empty");
  const _component_shop_car_list = common_vendor.resolveComponent("shop-car-list");
  (_component_base_empty + _component_shop_car_list)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: _ctx.status
  }, _ctx.status ? common_vendor.e({
    b: _ctx.status === 1
  }, _ctx.status === 1 ? {
    c: common_vendor.o((...args) => _ctx.goHome && _ctx.goHome(...args)),
    d: common_vendor.p({
      tip: "\u60A8\u8FD8\u6CA1\u6709\u6DFB\u52A0\u5546\u54C1"
    })
  } : {}, {
    e: _ctx.status === 2
  }, _ctx.status === 2 ? {
    f: common_vendor.o(_ctx.handleChange),
    g: common_vendor.o(_ctx.handleChangeCount),
    h: common_vendor.p({
      ["car-list"]: _ctx.shopCarInfo
    }),
    i: _ctx.allChecked ? 1 : "",
    j: common_vendor.o((...args) => _ctx.handleAllCheckedChange && _ctx.handleAllCheckedChange(...args)),
    k: common_vendor.t(_ctx.totalMoney),
    l: common_vendor.o((...args) => _ctx.goOrderPage && _ctx.goOrderPage(...args))
  } : {}) : {});
}
var MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/yihang/Desktop/1909A/bw-wxshop-client/src/pages/shopCar/index.vue"]]);
wx.createPage(MiniProgramPage);
