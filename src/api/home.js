import httpTool from "utils/http";

// 获取购物车数量 展示底部购物车导航小红点
export const getProdCount = () => httpTool.get('/p/shopCart/prodCount')

// 首页导航
export const getTagList = () => httpTool.get('/prod/tag/prodTagList');

// 首页通知
export const getTopNoticeList = () => httpTool.get('/shop/notice/topNoticeList');

// 首页轮播
export const getIndexImage = () => httpTool.get('/indexImgs')

// 首页列表数据
export const getProdListByTagId = (params = {tagId:1, size:6}) => httpTool.get('/prod/prodListByTagId', { 
  params 
})