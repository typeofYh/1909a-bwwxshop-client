import httpTool from "utils/http";

export const confirmOrder = (data) => httpTool.post('/p/order/confirm', data)


export const submitOrder = (data) => httpTool.post('/p/order/submit', data)

export const payParams = (data) => httpTool.post('/p/order/pay', data)