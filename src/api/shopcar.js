import httpTool from "utils/http";

export const shopCarInfo = (data) => httpTool.post('/p/shopCart/info', data)

export const shopCarTotalPay = (data = []) => httpTool.post('/p/shopCart/totalPay', data)

export const shopCarChangeItem = (data) => httpTool.post('/p/shopCart/changeItem', data)

