import {
	createSSRApp
} from "vue";
// console.log(vue);
import App from "./App.vue";
import Empty from "@/components/baseEmpty"
import ShopCarItem from "@/components/baseShopItem"
import "./utils/navigate"
export function createApp() {
	const app = createSSRApp(App);
	app.component('base-empty', Empty);
	app.component('base-shop-Item', ShopCarItem);
	// app.use(baseComponents, {
	// 	baseName: 'base'
	// })
	return {
		app,
	};
}


console.log('app');