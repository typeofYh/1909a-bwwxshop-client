export const setShopCarIconText = (text) => {
  uni.setTabBarBadge({
    index: 3,
    text: `${text}`
  })
}